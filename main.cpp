#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
//#include <boost/mpi/status.hpp>
#include <boost/mpi.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
//#include <boost/archive/text_oarchive.hpp>
//#include <boost/archive/text_iarchive.hpp>
#include <boost/mpi/collectives.hpp>
namespace mpi = boost::mpi;
using namespace boost::archive;


// might be useful to check universally unique identifiers values: https://theboostcpplibraries.com/boost.uuid
#include "CohortArc.h"
#include "Location.h"
#include "Cohort.h"
#include "GridCell.h"
#include "tags.h"
#include <random>
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <thread>
#include <chrono>
#include <sstream>
std::stringstream ss;
std::default_random_engine generatoruniform;

/*=========================================================================================================
												serialization functions
=============================================================================================================*/
template <typename Archive>
void serialize(Archive & ar, CohortArc & coh, const unsigned int version)
{
  ar & coh.ID_; // Cohort unique identifier;
  ar & coh.destinationCellId_;
}

/*=========================================================================================================
												parameters
=============================================================================================================*/
const int cellCounter = 33; // initial population size, make the initial population size double the number of territories
int simTime = 100;
int burnIn = 50;
long long newtagid; //note that max allowed value is 18 446 744 073 709 551 615 (I think it should be enough, but your time loop is in months so about 12000 * number of cores used;
long long tagrec;

int Cohort::numberOfCohorts = 0;
int GridCell::numberOfCells = 0;
std::vector<GridCell*> gclList;
typedef std::vector<int> int_vector;
typedef std::vector<GridCell*>::iterator TIter;
int getRandomNumber(int min, int max)
{
// Generate a random number between min and max (inclusive)
// Assumes srand() has already been called
// fun from learn.cpp webpage
    static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);  // static used for efficiency, so we only calculate this value once
    // evenly distribute the random number across our range
    return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}

std::default_random_engine generator;

void createCells()
//create the initial population
{
	for (int i = 0; i < cellCounter; ++i) gclList.push_back(new GridCell);

}

/*=============================================================================================================
												implementation
=============================================================================================================*/
int main (int argc, char *argv[])//() without irecv //int argc, char** argv) with basic mpi
{
//initialize the cells
	srand(static_cast<unsigned int>(time(0)));
	createCells();
  for (int ii = 0; ii < cellCounter; ++ii)
  {
    int ci = gclList[ii]->getGclID();
    gclList[ii]->fixCellID(ci);//make sure the element cellID within Cohort class is assigned the corresponding gridcell id
        /*for(int jj=0; jj< 2; jj++)// to check current cell Id is assigned correctly
        {
          std::cout << gclList[ii]->getpCoh()[jj]->getCurrentCellID() << std::endl;
        }*/
  }


  mpi::environment env{argc, argv};
  mpi::communicator world;

  /*=============================================================================================================
  												splitting cells between cores.
                                                TO DO: allow last core to only process the remainder + add option for when  world.size()==1
  =============================================================================================================*/
  if(world.size() < 3)
  {
    std::cerr << "warning: you need at last 3 cores to run the model with mpi\n";
    exit(1);
  }

  std::vector<GridCell*> gclList_rank; //vector of cells in each rank
  int interval = (cellCounter + 1) / world.size();
  int remainder = (cellCounter + 1) % world.size();
  int start;
  int end;
  std::vector<int> allr; allr.reserve(cellCounter); // allr sets and stores info about which gclID will be processed by each rank
  for(int countRank = 0; countRank < world.size(); countRank++)
  {
    if(countRank==0)
    {
      start = 0;
      end = interval-1;
      		for (int i = start; i < (end +  1); ++i) allr.push_back(countRank);
    }
    else if (countRank!=0 && countRank < (world.size()-1) )
    {
      start = countRank * interval;
      end = countRank * interval + interval -1;
      for (int i = start; i < (end +  1); ++i) allr.push_back(countRank);
    }
    else if (countRank == (world.size()-1))
    {
      start = countRank * interval;
      end = cellCounter - 1;
      for (int i = start; i <= end; ++i) allr.push_back(countRank);
    }
  }

  // based on the infos stored in allr copy to gclList_rank the grid cells processed in each rank but keep gclList to access infos about the other ranks.
  if(world.rank() == (world.size()-1))
  {
    gclList_rank.reserve((interval + remainder)); // the last rank is processing more cells, so reserve larger space
  }
  else
  {
    gclList_rank.reserve(interval);
  }
  //std::cout << "size gcllist " << gclList.size() << std::endl;
  for (int y = 0; y < allr.size(); y++)
  {
    gclList[y]->setRank(allr[y]);
    //std::cout << "gcl id " <<  gclList[y]->getGclID() << " rank " << gclList[y]->getRank() <<  std::endl;
    //copy to gclList_rank the cells I want to process here!
    if(world.rank()==gclList[y]->getRank())
    {
        gclList_rank.push_back(new GridCell(*(gclList[y])));
    }
  }
    //std::cout << "size gcllist after removal " << gclList_rank.size() << " in rank " << world.rank() << std::endl;

    // check that everything went well:
    //for (int y = 0; y < gclList_rank.size(); y++) std::cout << " world.rank()" << world.rank() << " gcl id " <<  gclList_rank[y]->getGclID() << " rank in the gclList" << gclList_rank[y]->getRank() <<  std::endl;


    //for (int i = 0; i < cellCounter; ++i) std::cout << "rank " << world.rank() << " cellid " << gclList[i]->getGclID() <<  "; and rank "  << gclList[i]->getRank() << std::endl;
    /*=============================================================================================================
                            creating all necessary tag ids to allow for different number of cores
    =============================================================================================================*/
    int seq = 0;
    std::vector<tags*> alltags; alltags.reserve(pow(world.size(), 2));
    for(int r = 0; r < world.size(); r++)
    {
      for(int tow = 0; tow < world.size(); tow++)
      {
        if(r != tow)
        {
          alltags.push_back(new tags(r, tow, seq));
          //if(world.rank()==1) std::cout << "tags from " << r << " to " << tow << " and seqid " << seq << std::endl;
          seq++;
        }
      }
    }
    /*=============================================================================================================
    												start biology within each core
    =============================================================================================================*/
   // Start calculating the biology
    ivector dispersalPool; dispersalPool.reserve(300000);
    ivector temp; temp.reserve(1000);
    std::vector<ivector> dispersalPool_rank(world.size(), temp);
    std::vector<CohortArc> tosend; tosend.reserve(10000);
    std::vector<CohortArc> temp2; temp2.reserve(1000);
    std::vector<std::vector<CohortArc>> toreceive(world.size(), temp2);


    int popSize[burnIn+simTime]; //store pop size
    std::string resRankId = "results_rank_" +  std::to_string(world.rank()) + ".csv"; //std::string
    std::ofstream fout_res(resRankId.c_str());
    if(world.rank() == 0) fout_res << "psize" << ", " << "time_step" << ", " <<	"GclID" << ", " << "CohortID" << ", " << "CohortRep" <<", " << "CohortSurvival" <<", " << "currentcell" << ", "
    << "CohortDispersal" << std::endl;

    for (int k = 0; k < 15; k++)//(burnIn+simTime)
    {
      std::cout << "TIME STEP " << k << " in rank "<< world.rank() << std::endl;
      int res=0;
      int popSize_t = 0;


      int endpoint = gclList_rank.size();//calculate size of gclList_rank to loop through
      for (int ii = 0; ii < endpoint; ii++)
      {
        //std::cout << "rank " << rank << " grid cell id actually processed " << gclList_rank[ii]->getGclID() << std::endl;
        gclList_rank[ii]->setGS();
        popSize_t = popSize_t + gclList_rank[ii]->getGS();
      }

      popSize[k] = popSize_t; // population size
      //std::cout << "population size " << popSize[k] << " at time " << k << " in rank " << rank << std::endl;

      if (popSize[k] == 0)
      {
        std::cerr << "warning: the population went extinct -> simulation terminated\n";
        exit(1);
      }

      for (int i = 0; i < endpoint; i++)
      {
        //------------- change all relevant variables before calculating the vital rates
        gclList_rank[i]->setGS(); // calculate group size
        //------------- calculate vital rates for each individual
        gclList_rank[i]->setVitalRates(popSize_t);
        gclList_rank[i]->setTotNumOff();
        //------------- calculate dispersing probability for each individual and get destination cell if any
        gclList_rank[i]->setMigrants();
        //------------- save to file ind ID, Cell ID, etc....
          for(int jj=0; jj<gclList_rank[i]->getGS(); jj++)
          {
             fout_res << popSize[k] << ", " << k << ", " <<
               //gcl level
               gclList_rank[i]->getGclID() << ", " <<
               // cohort level
               gclList_rank[i]->getpCoh()[jj]->getID() << ", " <<
               gclList_rank[i]->getpCoh()[jj]->getFertility() << ", " <<
               gclList_rank[i]->getpCoh()[jj]->getSurvival() << ", " <<
               gclList_rank[i]->getpCoh()[jj]->getCurrentCellID() << ", " <<
               gclList_rank[i]->getpCoh()[jj]->getDispersal() << ", " << std::endl;
           }
        //------------- add recruits to the territory
        gclList_rank[i]->addOffspring(world.rank(), gclList_rank[i]->getGclID());
        //------------- remove dead individuals
        gclList_rank[i]->removeDead();
        gclList_rank[i]->setGS();
        int gsbeforedispersal = gclList_rank[i]->getGS();
        //------------- sum number of migrants in each territory
        gclList_rank[i]->numMigrants();
        //------------- get migrants
        gclList_rank[i]->getMigrants();
        //-------------	create the dispersal pool
        for (int j=0; j < (gclList_rank[i]->getNumMigrants()); j++)  dispersalPool.push_back(new Cohort(*(gclList_rank[i]->migrants[j])));
          gclList_rank[i]->setGS();
          //std::cout << "group size before dispersal = " << gsbeforedispersal << "group size after dispersal = " << gclList_rank[i]->getGS() << std::endl;
        }// closing for (int i=start; i<=end; i++)
        // close file with results for the rank
        //std::cout << "size dispersal pool " << dispersalPool.size() << std::endl;

/*
    Possible improvement: no need to create intermediate dispersalPool vector.
    you can simply directly push to the dispersalPool_rank
*/

      /*=============================================================================================================
    						move dispersers between cells or send them to the relevant ranks
                            NB consider using isend irecv. It might make the computation a bit faster because then the order won't matter anymore
    check the option nonblocking branch and test performance
    =============================================================================================================*/
    /*****
    * first need to split the cohort between all the ranks they need to go to:
    * check this nice example: http://mpitutorial.com/tutorials/dynamic-receiving-with-mpi-probe-and-mpi-status/
    *****/

    int whereto;
    if(dispersalPool.size()>0)
    {
        //std::cout << "check initial  size of dispersalPool.size() " << dispersalPool.size() << " in rank " << world.rank() << std::endl;
        for(std::vector<Cohort*>::iterator j = dispersalPool.begin(); j != dispersalPool.end(); j++)
        {
          //randomly select a destination cell
          Cohort* moving2 = (*j);
          std::uniform_int_distribution<int> unif(1,cellCounter);
          int destination = unif(generatoruniform);
          moving2->setDestinationCell(destination);
          //std::cout << "in rank " << world.rank() << " cohort id " << moving2->getID() << " is moving to grid cell "<< moving2->getDestinationCell() << std::endl;

          //split the cohorts in a vector of vectors
          int dest = moving2->getDestinationCell();
          for(int cc = 0; cc<cellCounter; cc++)
          {
            if(dest == gclList[cc]->getGclID())
            {
              whereto = gclList[cc]->getRank();
            //  std::cout << "rank " << world.rank() << " moving cohort " << moving2->getID() << " from rank " << world.rank() << " to rank " << gclList[cc]->getRank() << std::endl;
            }
          }
          dispersalPool_rank[whereto].push_back(new Cohort(*(moving2)));
          delete moving2;
          moving2 = NULL;
        }
        dispersalPool.clear();
        //std::cout << "check final size of dispersalPool.size() " << dispersalPool.size() << " in rank " << world.rank() << std::endl;
    }//closing parenthesis for if(dispersalPool.size()>0){

      /*****
      * loop through the vectors that need to be sent, and send them around vector by vector...
      *****/
    for (int rankto = 0; rankto < world.size(); rankto++)//each vector of Cohort contsins Cohorts moving to the same "rankto"
    {
      //std::cout << "rank " << world.rank() << " dispersalPool_rank[rankto].size() " << dispersalPool_rank[rankto].size() << " whereto " << rankto << std::endl;

        if(rankto == world.rank()) // if the destination cell is processed within the same rank, simply rebase the Cohort
        {
          if(dispersalPool_rank[rankto].size()>0)
          {
            //std::cout << " secondo rank " << world.rank() << " dispersalPool_rank[rankto].size() " << dispersalPool_rank[rankto].size() << " whereto " << rankto << std::endl;
            for(std::vector<Cohort*>::iterator jk = dispersalPool_rank[rankto].begin(); jk != dispersalPool_rank[rankto].end(); jk++)
            {
              Cohort * movingto = (*jk);
              //std::cout << "initially you have cohort " << movingto->getID() << " moving to " << movingto->getDestinationCell() << std::endl;
              //std::cout << "endpoint " << endpoint << " in rank " << world.rank() << std::endl;
              for (std::vector<GridCell*>::iterator l = gclList_rank.begin(); l != gclList_rank.end(); l++)
              {
                  if( (*l)->getGclID() == movingto->getDestinationCell() )
                  {
                    (*l)->addCohort(*movingto);
                    //std::cout << "rebasing cohort" << movingto->getID() << " within same rank (" << world.rank() << ") and to gclid " << (*l)->getGclID() << std::endl;
                  }

                  /*else
                  {
                  write something meaningful that checks if all the cells are wrong....
                    std::cout << "error. trying to rebase a cohort within same rank, but there is no corresponding cell!"
                  }*/
                //std::cout << "rebasing cohort" << moving->getID() << " within same rank and to gclid " << gclList_rank[l]->getGclID() << std::endl;
              }
              // remove cohort after dispersal within same rank
              //dispersalPool_rank[rankto].erase(next(begin(dispersalPool_rank[rankto]), + jk));
              delete movingto;
              movingto = NULL;

              //std::cout << "dimensions rebasing dispersalPool_rank " << rankto << " is " << dispersalPool_rank[rankto].size() << std::endl;
            }
          }
        }
        else // else means rankto!=world.rank(); send dispersing cohorts around
        {
          if(dispersalPool_rank[rankto].size()>0)
          {
            //std::cout << " in rank " << world.rank() << " dispersalPool_rank[rankto].size() " << dispersalPool_rank[rankto].size() << " whereto " << rankto << std::endl;
            for(std::vector<Cohort*>::iterator jk = dispersalPool_rank[rankto].begin(); jk != dispersalPool_rank[rankto].end(); jk++)
            {
              Cohort * movingto = (*jk);
              tosend.push_back(CohortArc(movingto->getID(), movingto->getDestinationCell()));
              delete movingto;
              movingto = NULL;
            }
          }

          //find out what is the correct tagid
          for(std::vector<tags*>::iterator tta = alltags.begin(); tta != alltags.end(); tta++)
                  if ((*tta)->getFrom() == world.rank() && (*tta)->getTo() == rankto)  newtagid = (*tta)->getTagid();

          //std::cout << "from " << world.rank() << " to " << rankto << " tag id " << newtagid << std::endl;
          newtagid = newtagid + ( ( pow(world.size(), 2) - world.size() ) * k ); // change the value of newtagid to ensure having unique tag ids through the loop
          world.send(rankto, newtagid, tosend);

          //std::cout << "send done from " << world.rank() << " to rank " << rankto << " for number " << tosend.size() << " of cohorts" << std::endl;
          tosend.clear();//if(tosend.size()>0)

        }

      dispersalPool_rank[rankto].clear();
    }
    /*=============================================================================================================
                        receive dispersers
  =============================================================================================================*/
    //mpi::status statuses[world.size()];
    for(int st = 0; st < world.size(); st++)
    {
      //find out what is the correct tagid
      for(std::vector<tags*>::iterator tta = alltags.begin(); tta != alltags.end(); tta++)
      {
        if ( (*tta)->getTo() == world.rank() && (*tta)->getFrom() == st && st != world.rank())
        {
          tagrec = (*tta)->getTagid(); //within the time loop you could multiply by *(k+1)
          tagrec = tagrec + ( ( pow(world.size(), 2) - world.size() ) * k ); // change the value of tagrec to ensure having unique tag ids through the loop
          if(world.rank()==4) std::cout << "time " << k<< "/ receiving from rank " << st << " in rank " << world.rank() <<  " with tag id " << tagrec << std::endl;
          world.recv(st, tagrec, toreceive[st]);
        }

      }
    }
  /*****
  * once dispersers are received by the corresponding rank, reassign them to the correct grid cell id
  *****/
  for(int st = 0; st < world.size(); st++)
  {
    if(toreceive[st].size()>0)
    {
      for(std::vector<CohortArc>::iterator dimrec = toreceive[st].begin(); dimrec != toreceive[st].end(); dimrec++)
      {
        Cohort * cohreceived = new Cohort((*dimrec).name(), (*dimrec).dest());
        for (std::vector<GridCell*>::iterator l = gclList_rank.begin(); l != gclList_rank.end(); l++)
        {
          if( (*l)->getGclID() == cohreceived->getDestinationCell() )
          {
            (*l)->addCohort(*cohreceived);
            //std::cout << "in world.rank " << world.rank() << " adding cohort" << cohreceived->getID() << " to a new rank and to gclid " << (*l)->getGclID() << std::endl;
          }
          /*else
          {
          maybe write something meaningful that check if all the cells are wrong....
          std::cout << "error. trying to rebase a cohort within same rank, but there is no corresponding cell!"
          }*/
        }
        delete cohreceived;
        cohreceived = NULL;
      }
    }
    toreceive[st].clear();
  }

  // make sure you're not loosing dispersers somewhere
  for (int rankto = 0; rankto < world.size(); rankto++)//each vector of Cohort contains Cohorts moving to the same "rankto"
  {
    if(dispersalPool_rank[rankto].size() > 0) std::cout << " something went wrong. there are missing cohorts that need to disperse from rank " << world.rank() << std::endl;

  }

  /*=============================================================================================================
      ISSUE: how can I be sure that for a rank the time loop is ended for all ranks before goes on?
      Send a zero-sided vector.
=============================================================================================================*/
  std::string s = "finishing_time_" + std::to_string(k) + "_in_rank_" + std::to_string(world.rank());
  //boost::mpi::broadcast(world, s, 0);
  //std::cout << s << std::endl;

  //find out what is the correct tagid
  for(std::vector<tags*>::iterator tta = alltags.begin(); tta != alltags.end(); tta++)
          if ((*tta)->getFrom() == world.rank() && (*tta)->getTo() == world.rank())  newtagid = (*tta)->getTagid();


} //close time loop

fout_res.close();


/*=============================================================================================================
                    reads in all the csv files created by a single rank and makes a single one out of them...
                    on the HPC cluster you need to specify the path....
=============================================================================================================*/

  if(world.rank() == 0)
  {
    std::this_thread::sleep_for(std::chrono::seconds(30));
    //create a file where to append the results later
    std::ofstream outputfile("all_gridcell.csv", std::ios::out | std::ios::app);
    for (int ran = 0; ran < world.size(); ran++)
    {
      std::string readRank = "results_rank_" +  std::to_string(ran) + ".csv";
      std::ifstream workers(readRank.c_str(), std::ios::in);
      if(workers.is_open())
      {
        outputfile << workers.rdbuf();
        std::cout << "merged " << readRank << std::endl;
      }
      else
      {
        std::cout << "WARNINGS!!!!!! file " << readRank << " cannot be opened" << std::endl;
      }
      workers.close();
    }
    outputfile.close();
  }

return 0;

}// closing main
