# Description

A fundamental biological model describing the fate of cohorts of individuals that survive, reproduce and disperse. Time and space are discrete variables.

The model consists of classes: cohorts of individuals live within grid cells, and each grid cell is characterized by latitude and longitude. The classes are nested, such that a `std::vector<cohorts*>` belongs to a Grid Cell. The model runs for multiple years, and at each time step, there is dispersal such that the cohorts of individuals move randomly between grid cells.

# Parallelization

The model is written in C++ and parallelized with MPI and the boost library to allow computation on multiple nodes in an HPC cluster.

On Linux, to create an executable use:

- `mpic++ -I/$HOME/boost_1_61_0/boost/mpi -std=c++11 -Wfatal-errors CohortArc.h tags.h GridCell.cpp Cohort.cpp Location.cpp main.cpp -Llibdir -lboost_mpi -lboost_serialization -lboost_locale -o out` (Tested on g++ (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0)

Run the executable with:

- `mpirun -np 7 out`

(note that you need at least three cores for the model to compile correctly)

To run the model on an HPC cluster, use the bash scripts execute.sh and compile.sh. You might need to modify the partition and modules loaded.

# License

Copyright 2018 Michela Busana

The code in this repository is licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
