#ifndef Classes_GridCell_h
#define Classes_GridCell_h

#include<iostream>
#include <fstream>
#include<vector>
#include<iterator>
#include <algorithm>
#include <functional>
#include "Cohort.h"
//#include "Parameter.h"

class Cohort;
//class Parameter;

typedef std::vector<Cohort *> ivector;
typedef std::vector<Cohort *>::iterator EIter;
typedef std::vector<int> int_vector;


class GridCell
{
public:
		GridCell();
		GridCell(const GridCell &);
		~GridCell();
		void operator=(const GridCell &);
		int getGclID(); // function
		static int numberOfCells;	// static member to keep track of all territories created and assign an unique ID identifier to each
		void addCohort(const Cohort &nInd); // function add an Cohort from the dispersal pool to a GridCell
    void setVitalRates(int psize_t); // function makes individuals in a GridCell go through reproducion and survival. calls Cohort::setSurvival()
    void setMigrants(); // function makes individuals in a GridCell disperse
    void addOffspring(int rank, int cellid); // function adds the offspring produced in a GridCell to the same GridCell
		void removeDead(); // function to remove dead Cohorts from a GridCell
		void getMigrants(); //function  selects dispersing  Cohort from a GridCell with removal
		void numMigrants(); //function set numMigrants by counting the number of dispersing returns Cohort from a GridCell
		int getNumMigrants(); // function returns the number of migrants from a GridCell
		int getTotNumOff(); // function returns the total number of offspring prouced in a GridCell in a given year
		void setTotNumOff(); // function calculates the total number of offspring produced in a GridCell in a given year
		ivector getpCoh();
		ivector migrants; // store migrants from a GridCell; could this be protected?
		int getGS();
		void setGS();
		void setRank(int rank);
		int getRank();
    void fixCellID(int ci);

protected:
		ivector pCoh; // store all Cohorts in a GridCell
		int nMigrants; // number of migrants in a GridCell
		int gclID; // GridCell ID
		int GroupSize; // number of Cohorts within a cell
		int totNumOff;
		int worker; // process id where the cell biology is executed

private:
		void free();
		void copy(const GridCell &);
		void initCoh(int rank, int cellid);
};

#endif
