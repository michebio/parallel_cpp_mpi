#ifndef Cohort_h
#define Cohort_h

#include <boost/mpi/datatype.hpp>
#include <string>
#include <iostream>
#include <vector>
#include "Location.h"
#include "CohortArc.h"

class Cohort
{
public:
		Cohort(int rank, int cellid);
		Cohort(std::string id, int dest);
		Cohort(const Cohort &); // copy constructor
		void operator=(const Cohort &);
		~Cohort(); //deconstructor
		int getFertility();
		int getNumOff();
		int getSurvival();
		int getDispersal();
		std::string getID();
		static int numberOfCohorts; // static member to keep track of all Cohorts created and assign an unique ID identifier to each

		void setFertility(float intercept, float slopePsize, int psize);// function to get fertility rates
		void setSurvival(float intercept); // function to get survival probability
		void setDispersal(float intercept); // function to calculate dispersal probability
		void setNumCohTerr();
		void setCurrentCellID(int cellid);
		int getCurrentCellID();
		void setDestinationCell(int cellid);
		int getDestinationCell();

protected:
    std::string ID; // Cohort unique identifier;
		int fertility; // does an Cohort reproduces? 1=yes, 0=no
		int numOff; // number of offspring produced
		int survival; // survival yes/no(1/0) to the next time step
		int dispersal; // dispersal yes/no(1/0)
		int currentCellId;
		int destinationCellId;
		Location mCurrentLocation;
    Location mDestinationLocation;

private:
		void free();
		void copy(const Cohort &);
};
#endif
