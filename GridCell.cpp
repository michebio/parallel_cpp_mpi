#include "GridCell.h"
#include "Cohort.h"
#include <algorithm>
#include <functional>
#include <random>
#include <iostream>
#include <vector>
#include <fstream>
#include <iterator>

std::default_random_engine generatorunif;

class Cohort;

GridCell::GridCell() // default constructor
{
	pCoh.reserve(30);
	initCoh(-1, -1);
	migrants.reserve(30);
	nMigrants = 0;
	gclID = numberOfCells + 1;
	numberOfCells++;
	worker = 0;
	GroupSize = 0; // number of Cohorts within a cell
	totNumOff = 0;
}
GridCell::GridCell(const GridCell &gcl) // copy constructor
{
	copy(gcl);
	//pstd::cout << "GridCell cc'tor" << std::endl;
}
GridCell::~GridCell() //deconstructor
{
	free();
	std::cout << "GridCell deconstructor" << std::endl;
}
void GridCell::operator=(const GridCell &gcl)
{
	if (this != &gcl)
	{
		free();
		copy(gcl);
	}
	std::cout << "GridCell: assign" << std::endl;
}
void GridCell::free()
{
	for (int i=0; i<pCoh.size(); i++)
	{
		delete pCoh[i];
		pCoh[i] = NULL;
	}
	pCoh.clear();
}
void GridCell::copy(const GridCell &gcl)
{
	gclID = gcl.gclID;
	nMigrants = gcl.nMigrants;
	worker = gcl.worker;
	GroupSize = gcl.GroupSize; // number of Cohorts within a cell
	totNumOff = gcl.totNumOff;
	for(std::vector<Cohort *>::const_iterator it = gcl.pCoh.begin(); it < gcl.pCoh.end(); ++it) pCoh.push_back(new Cohort(*(*it)));

}
int GridCell::getGclID() { return gclID; } // function returns unique GridCell ID
void GridCell::initCoh(int rank, int cellid)
{
//initialize each GridCell with 2 cohorts
	int size = 10;//pCoh.size();
	for (int i = 0; i < size; i++)
	{
		Cohort *p1Ind = new Cohort(rank, cellid);
		pCoh.push_back(p1Ind);
	}
}
void GridCell::addCohort(const Cohort &nInd)
{
// function to add an individual to a GridCell
	pCoh.push_back(new Cohort(nInd));
}
void GridCell::setVitalRates(int psize_t)
{
// function makes individuals in a GridCell go through reproducion and survival
	for(EIter i=pCoh.begin(); i!=pCoh.end(); i++)
	{
	// change the values here if you want to play with different parameter values
		(*i)->setFertility(0.7, -0.0000002, psize_t);
		(*i)->setSurvival(0.6);
	}
}
void GridCell::fixCellID(int ci)
{
	for(EIter i=pCoh.begin(); i!=pCoh.end(); i++) {
		(*i)->setCurrentCellID(ci);
	}
}

void GridCell::setMigrants()
{
// function makes individuals in a GridCell disperse
	for(EIter i=pCoh.begin(); i!=pCoh.end(); i++)
	{
		(*i)->setDispersal(0.3);
	} //close for loop
}


void GridCell::addOffspring(int rank, int cellid)
{
// function adds the offspring produced in a GridCell to the same GridCell
	int off = 0;
	for(EIter i=pCoh.begin(); i!=pCoh.end(); i++)
	{
		int count = (*i)->getNumOff();
		off = off+count;
	}
	while (off>0)
	{
		pCoh.push_back(new Cohort(rank, cellid));
		off--;
	}
}
void GridCell::removeDead()
{
// function removes dead individuals from a GridCell
	for(EIter it=pCoh.begin(); it!=pCoh.end();)
	{
	if( (*it)->getSurvival()==0 )
		{
			delete * it;
			*it = NULL;
			it=pCoh.erase(it);
		}
		else
    		++it;
	}
}

void GridCell::getMigrants()
{
//function removes dispersing Cohorts from a GridCell and copies them to the std::vector<Cohort*> migrants
	ivector inds;
	for(EIter it=pCoh.begin(); it!=pCoh.end();)
	{
	if( (*it)->getDispersal()==1 )
		{
			inds.push_back(new Cohort( **it)); //copy the individual to the migrants vector
			delete * it;
			*it = NULL;
			it=pCoh.erase(it);
		}
		else
    		++it;
	}
	migrants = inds;
}
void GridCell::numMigrants()
{
// function counts total number of migrants in a GridCell
	int num = 0;
	for(EIter i=pCoh.begin(); i!=pCoh.end(); i++)
	{
		int count = (*i)->getDispersal();
		num = num+count;
	}
	nMigrants = num;
}
int GridCell::getNumMigrants()
{
//function returns number of migrants in a GridCell
	return nMigrants;
}
int GridCell::getTotNumOff() { return totNumOff; } // function returns the total number of offspring in a GridCell
void GridCell::setTotNumOff()
{
// function counts total number of offspring produced in a GridCell
	int num = 0;
	for(EIter i=pCoh.begin(); i!=pCoh.end(); i++)
	{
		int count = (*i)->getNumOff();
		num = num+count;
	}
	totNumOff = num;
}
ivector GridCell::getpCoh()
{
  return pCoh;
}


int GridCell::getGS() { return GroupSize; }

void GridCell::setGS()
{
// function calculates the total number of individuals in a territory
    int num = 0;
	for(EIter i=pCoh.begin(); i!=pCoh.end(); i++) num++;
		GroupSize = num;
}
void GridCell::setRank(int rank)
{
	worker = rank;
}
int GridCell::getRank()
{
	 return worker;
}
