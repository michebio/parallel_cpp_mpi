#include "Cohort.h"
#include "Location.h"
#include "CohortArc.h"
#include <boost/mpi/datatype.hpp>

#include <string>
#include <iostream>
#include <vector>
#include <random>
#include <fstream>

// important! set the ID in the Cohort constructor to be an unique value, so a combination of rank and counter for cohorts
std::default_random_engine generator1;
std::default_random_engine generator2;
std::default_random_engine generator3;
std::default_random_engine generator4;


Cohort::Cohort(int rank, int cellid)
{
	std::normal_distribution<float> normal(0,3);
	fertility = 0;
	numOff = 0;
	survival = 1;
	dispersal = 0;
	ID = std::to_string(rank) + "_" + std::to_string(numberOfCohorts + 1);  //is it possible to set this as constant?
	numberOfCohorts++;
	currentCellId = cellid;
	destinationCellId = cellid;
	mCurrentLocation.SetIndices( cellid, cellid );
	mDestinationLocation = mCurrentLocation;
}

//Constructor where you pass the CohortArc infos as argument:
Cohort::Cohort(std::string id, int dest)
{
	fertility = 0;
	numOff = 0;
	survival = 1;
	dispersal = 0;
	ID = id;
	currentCellId = dest;
	destinationCellId = dest;
	mCurrentLocation.SetIndices( dest, dest );
	mDestinationLocation = mCurrentLocation;
}

Cohort::Cohort(const Cohort &ind)
{
	copy(ind);
	//std::cout << "ind copy cc'tor" << std::endl;
}
void Cohort::copy (const Cohort &ind)
{
		fertility = ind.fertility; // does an individual reproduces? 1=yes, 0=no
		numOff = ind.numOff; // number of offspring produced
		survival = ind.survival; // survival yes/no(1/0) to the next time step
		dispersal = ind.dispersal; // dispersal yes/no(1/0)
		ID = ind.ID; // individual identifier
		currentCellId = ind.currentCellId;
		destinationCellId = ind.destinationCellId;
		mCurrentLocation = ind.mCurrentLocation;
		mDestinationLocation = ind.mDestinationLocation;
}
void Cohort::operator=(const Cohort &ind)
{
	if (this != &ind)
	{
		free();
		copy(ind);
	}
}
Cohort::~Cohort() { free(); }
void Cohort::free() {}
std::string Cohort::getID() { return ID; } // function returns individual unique identifier ID
int Cohort::getFertility() { return fertility; } // functions returns 1 if an individual recruits offspring, 0 otherwise
int Cohort::getNumOff() { return numOff; } // if fertility=1, function returns the number of offspring produced by an individual
int Cohort::getSurvival() { return survival; } // function returns 1 if an individual survives; 0 otherwise
int Cohort::getDispersal() { return dispersal; } // function returns 1 if an individual dispers; 0 otherwise
void Cohort::setFertility(float intercept, float slopePsize, int psize)
{
// function sets fertility to 1 if an individual recruits offspring, 0 otherwise
// it generates a binomial random value given a probability g
// for the moment I assume that only females reproduces
	float	g = intercept;
  	std::binomial_distribution<int> binomial(1, g);
    int res = binomial(generator2);
    fertility = res;
	numOff = res;
    //  std::cout << "set fertility " <<   fertility  << std::endl;
}
void Cohort::setSurvival(float intercept)
{
// function sets survival to 1 if an individual survives, 0 otherwise
// it generates a binomial random value given a probability p
	float	p = intercept;
  	std::binomial_distribution<int> binomial(1, p);
    int res = binomial(generator3);
    survival = res;
}
void Cohort::setDispersal(float intercept)
{
// function sets dispersal to 1 if an individual dispersers, 0 otherwise
// it is conditional on survival
// it generates a binomial random value given a probability p
// later we could make p dependendent on stage, sex, body condition, etc...
	float p = intercept;
  	std::binomial_distribution<int> binomial(1, p);
      int res = binomial(generator4);
      dispersal = (survival==1 ? res : 0); // if an individual survives then it can disperse
}


void Cohort::setCurrentCellID(int cellid)
{
	currentCellId = cellid;
}
int Cohort::getCurrentCellID()
{
	return currentCellId;
}
void Cohort::setDestinationCell(int cellid)
{
destinationCellId = cellid;
}
int Cohort::getDestinationCell()
{
	return destinationCellId;
}
