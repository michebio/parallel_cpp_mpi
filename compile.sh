#!/bin/bash
#SBATCH --partition=broadwell_short
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --time=10:00
#SBATCH --job-name=compile
#SBATCH --mem=100MB
#SBATCH --output copile_%J.out
#SBATCH --mail-type=END


export TMPDIR=/scratch
mkdir -p $TMPDIR

module load eb
module load Boost/1.65.1-foss-2017b

mpic++ -std=c++11 -Wfatal-errors CohortArc.h tags.h GridCell.cpp Cohort.cpp Location.cpp main.cpp -Llibdir -lboost_mpi -lboost_serialization -lboost_locale -o out_foss

# the alternative is Boost/1.61.0-intel-2016b with mpiicc -std=c++11 -Wfatal-errors ex_parallel.cpp -Llibdir -lboost_mpi -lboost_serialization -lboost_locale -o out_intel

