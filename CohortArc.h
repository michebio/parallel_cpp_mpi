#ifndef CohortArc_h
#define CohortArc_h
// this class is meant to wrap the info of dispersing Cohort that need to be moved between cores 

#include "Cohort.h"
#include "Location.h"

#include <boost/mpi/datatype.hpp>
#include <boost/serialization/access.hpp>
#include <boost/serialization/string.hpp>

//to serialize pointers use:
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include <string>
#include <iostream>
//#include <sstream>
#include <vector>

using namespace boost::archive;


class CohortArc
{
public:
	  CohortArc() = default;
		CohortArc(std::string name, int dest):
			ID_{std::move(name)}, destinationCellId_{dest} {}
		int dest() const { return destinationCellId_;}
		const std::string &name() const { return ID_;}

private:
	  friend class boost::serialization::access;

		template<class Archive>
		friend void serialize(Archive & ar, CohortArc & coh, const unsigned int version);
		std::string ID_; // Cohort unique identifier;
		int destinationCellId_;
		// when you need a location here add the values for lat and long
};


#endif
