#ifndef tags_h
#define tags_h
//this class simply keeps track of the tag ids to be passed to world.send() & world.recv()
class tags
{
public:
	  tags(int fr, int t, int id):
			from{fr}, to{t}, tagid{id} {}
		int getTagid() { return tagid;}
		int getFrom() { return from;}
		int getTo() { return to;}

		void setTagid(int id) { tagid = id; }
		void setFrom(int fr) {from = fr;}
		void setTo(int t) {to = t;}
private:
		int from;
		int to;
		int tagid;
};


#endif
