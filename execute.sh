#!/bin/bash
#SBATCH --partition=broadwell_short
#SBATCH --nodes=2
#SBATCH --ntasks=6
#SBATCH --time=10:00
#SBATCH --job-name=testmpi
#SBATCH --mem=1GB
#SBATCH --output testmpi_%J.out
#SBATCH --mail-type=END
#SBATCH --mail-user=m.busana@science.ru.nl


export TMPDIR=/scratch
mkdir -p $TMPDIR

srun -n 6 out_foss

# check performance with 
# sacct -o JobID,State,AllocNodes,NNodes,AllocCPUS,ncpus,ReqMem,MaxRSS --jobs=
